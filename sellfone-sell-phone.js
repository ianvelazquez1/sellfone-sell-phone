import { html, LitElement } from 'lit-element';
import style from './sellfone-sell-phone-styles.js';

class SellfoneSellPhone extends LitElement {
  static get properties() {
    return {
      brandArray:{
        type:Array,
        attribute:'brand-array'
      },
      optionsFlags:{
        type:Object
      },
      brandProducts:{
        type:Array
      },
      sellItem:{
        type:Array
      },
      productTypes:{
        type:Array
      },
      phoneConditions:{
        type:Array
      }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.sellItem=[];
    this.productTypes=[];
    this.phoneConditions=[
      {title:'Nuevo',images:['https://img.icons8.com/dusk/128/000000/gift.png']},
      {title:'Grado A',images:['https://img.icons8.com/dusk/128/000000/good-quality.png']},
      {title:'Grado B',images:['https://img.icons8.com/cotton/128/000000/olympic-medal-silver.png']},
      {title:'Grado C',images:['https://img.icons8.com/dusk/128/000000/used-product.png']}
    ];
    this.optionsFlags={
      firstSelected:false,
      secondSelected:false,
      thirdSelected:false,
      fourthSelected:false,
      fifthSelected:false
    };

    this.brandArray=[
      {
        brandName:'Samsung',
        brandImage:'https://technave.com/data/files/mall/article/201810260801452450.jpg',
        brandProducts:[
          {
            id: 'S23R2',
            title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
            detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
            images: [
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/frontal.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/perspectiva.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/lateral.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/trasera.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/dual.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg'
            ],
            price: 2999,
            discountedPrice: 2350,
            productTypes:[
              {title:'Celular Samsung Galaxy S7 Flat verde',images:['https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/frontal.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg']}
            ]
          },
        ]
      },{
        brandName:'IPhone',
        brandImage:'https://images.idgesg.net/images/article/2018/09/apple-iphone-xs-xr-lineup-100771848-orig.jpg',
        brandProducts:[
          {
            id: 'S23R2',
            title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
            detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
            images: [
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/frontal.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/perspectiva.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/lateral.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/trasera.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/dual.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg'
            ],
            price: 2999,
            discountedPrice: 2350
          }
        ]
      },{
        brandName:'Otras marcas',
        brandImage:'https://technave.com/data/files/mall/article/201810260801452450.jpg',
        brandProducts:[
          {
            id: 'S23R2',
            title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
            detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
            images: [
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/frontal.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/perspectiva.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/lateral.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/trasera.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
                'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/dual.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg'
            ],
            price: 2999,
            discountedPrice: 2350
          }
        ]
      }
    ]
  }

  render() {
    return html`
        <div class="selectionContainer">
               ${this.sellItem[0]?html`
               <h3>Marca</h3>
               <div class="listItem">
                 <p class="sectionTitle">${this.sellItem[0]}</p>
               </div>
               `
               :''
              }
              ${this.sellItem[1]?html`
              <h3>Modelo</h3>
               <div class="listItem">
                 <p class="sectionTitle">${this.sellItem[1].title}</p>
               </div>
              `:''}
              ${this.sellItem[2]?html`
              <h3>Versión</h3>
               <div class="listItem">
                 <p class="sectionTitle">${this.sellItem[2].title}</p>
               </div>
              `:''}
              ${this.sellItem[3]?html`
              <h3>Condición</h3>
               <div class="listItem">
                 <p class="sectionTitle">${this.sellItem[3].title}</p>
               </div>
              `:''}
            </div>
        </div>
         <div class="container">
           <div class="brandContainer">
           ${this.optionsFlags.firstSelected?''
            :html`<h3 class="title">Marca del teléfono</h3>`
            }
             ${this.optionsFlags.firstSelected?'':this._createBigCards()}
             ${this.optionsFlags.secondSelected?html`<h3 class="title">Modelo del teléfono</h3>`:''
            }
             ${this.optionsFlags.secondSelected?this._createCard(this.brandProducts):''}
             ${this.optionsFlags.thirdSelected?html`<h3 class="title">Versión del teléfono</h3>`:''
            }
             ${this.optionsFlags.thirdSelected?this._createCard(this.productTypes):''}
             ${this.optionsFlags.fifthSelected?html`<h3 class="title">Condiciones del teléfono</h3>`:''
            }
             ${this.optionsFlags.fifthSelected?this._createCard(this.phoneConditions):''}
           </div>
         </div>
      `;
    }

  firstUpdated(changedProperties){

  }



  _createBigCards(){
    return html`
      ${this.brandArray.map(brand=>{
        return html`
        <div class="bigCard" @click="${(e)=>this.selectBrand(brand)}">
          <img src="${brand.brandImage}" class="brandImage" alt="">
          <p class="brandTitle">${brand.brandName}</p>
        </div>`;
      })}
    `;
  }

  selectBrand(data){
    this.optionsFlags.firstSelected=true;
    this.optionsFlags.secondSelected=true;
    this.brandProducts=data.brandProducts;
    this.sellItem.push(data.brandName);
    this.update();
  }

  _createCard(object){
    return html`
     ${object.map(products=>{
       return html`
          <div class="smallCard" @click="${(e)=>this.selectProduct(products)}">
              <img class="productImage" src="${products.images[0]}">
              <p class="productTitle">${products.title}</p>
          </div>
       `;
     })}
    `;
  }

  selectProduct(data){
    if(!this.optionsFlags.thirdSelected){
      this.optionsFlags={
        firstSelected:true,
        secondSelected:false,
        thirdSelected:true
      };
    }else{
      this.optionsFlags={
        firstSelected:true,
        secondSelected:false,
        thirdSelected:false,
        fourthSelected:true,
        fifthSelected:false
      };
    }if(this.optionsFlags.fourthSelected){
      this.optionsFlags={
        firstSelected:true,
        secondSelected:false,
        thirdSelected:false,
        fourthSelected:false,
        fifthSelected:true
      };
    }
     
    this.sellItem.push(data);
    this.productTypes=data.productTypes;
    this.update();
  }

}

window.customElements.define("sellfone-sell-phone", SellfoneSellPhone);
