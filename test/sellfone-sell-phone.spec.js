/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-sell-phone.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-sell-phone></sellfone-sell-phone>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
